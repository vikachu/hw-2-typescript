import { fightersDetails, fighters } from "./mockData";
import { FighterDetail, Fighter } from "../interfaces/fighter";

type Method = "GET" | "POST" | "PUT" | "DELETE";

const API_URL =
  "https://api.github.com/repos/binary-studio-academy/stage-2-es6-for-everyone/contents/resources/api/";
const useMockAPI = true;

async function callApi<T>(endpoint: string, method: Method): Promise<T> {
  const url = API_URL + endpoint;
  const options = {
    method,
  };

  return useMockAPI
    ? fakeCallApi(endpoint)
    : fetch(url, options)
        .then((response) =>
          response.ok
            ? response.json()
            : Promise.reject(Error("Failed to load"))
        )
        .then((result) => JSON.parse(atob(result.content)))
        .catch((error) => {
          throw error;
        });
}

async function fakeCallApi(
  endpoint: string
): Promise<Fighter[] | FighterDetail> {
  const response =
    endpoint === "fighters.json" ? fighters : getFighterById(endpoint);

  return new Promise<Fighter[] | FighterDetail>((resolve, reject) => {
    setTimeout(
      () => (response ? resolve(response) : reject(Error("Failed to load"))),
      500
    );
  });
}

function getFighterById(endpoint: string): FighterDetail | undefined {
  const start = endpoint.lastIndexOf("/");
  const end = endpoint.lastIndexOf(".json");
  const id = endpoint.substring(start + 1, end);

  return fightersDetails.find((it) => it._id === id);
}

export { callApi };
