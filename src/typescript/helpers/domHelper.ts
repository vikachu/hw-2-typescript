import { Element } from "../interfaces/html_element";

export function createElement({
  tagName,
  className,
  attributes = {},
}: Element) {
  const element = document.createElement(tagName);

  if (className) {
    const classNames: string[] = className.split(" ").filter(Boolean);
    element.classList.add(...classNames);
  }

  Object.keys(attributes).forEach((key) =>
    element.setAttribute(key, attributes[key])
  );

  return element;
}
