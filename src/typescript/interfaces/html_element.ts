export interface Element {
  tagName: "div" | "span" | "img" | "button";
  className?: string;
  attributes?: { [index: string]: string };
}
