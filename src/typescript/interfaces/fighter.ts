export interface Fighter {
  _id: string;
  name: string;
  source: string;
}

export interface FighterDetail extends Fighter {
  health: number;
  attack: number;
  defense: number;
}
