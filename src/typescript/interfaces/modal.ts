export interface Modal {
  title: string;
  bodyElement: HTMLElement;
  onClose: CallableFunction;
}
