import { createElement } from "../helpers/domHelper";
import { FighterDetail } from "../interfaces/fighter";

export function createFighterPreview(
  fighter: FighterDetail,
  position: "left" | "right"
) {
  const positionClassName =
    position === "right" ? "fighter-preview___right" : "fighter-preview___left";
  const fighterElement = createElement({
    tagName: "div",
    className: `fighter-preview___root ${positionClassName}`,
  });

  const { name, health, attack, defense } = fighter;

  const fighterImage = createFighterImage(fighter);
  const fighterName = createElement({
    tagName: "span",
    className: "fighter-preview___name",
  });
  const fighterHealth = createElement({
    tagName: "span",
    className: "fighter-preview___health",
  });
  const fighterAttack = createElement({
    tagName: "span",
    className: "fighter-preview___attack",
  });
  const fighterDefense = createElement({
    tagName: "span",
    className: "fighter-preview___defense",
  });

  fighterName.innerText = name;
  fighterHealth.innerText = `Health: ${health}`;
  fighterAttack.innerText = `Attack: ${attack}`;
  fighterDefense.innerText = `Defense: ${defense}`;
  fighterElement.append(
    fighterImage,
    fighterName,
    fighterHealth,
    fighterAttack,
    fighterDefense
  );

  return fighterElement;
}

export function createFighterImage(fighter: FighterDetail) {
  const { source, name } = fighter;
  const attributes = {
    src: source,
    title: name,
    alt: name,
  };
  const imgElement = createElement({
    tagName: "img",
    className: "fighter-preview___img",
    attributes,
  });

  return imgElement;
}
