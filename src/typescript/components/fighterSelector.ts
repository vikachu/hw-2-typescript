import { createElement } from "../helpers/domHelper";
import { renderArena } from "./arena";
import versusImg from "../../../resources/versus.png";
import { createFighterPreview } from "./fighterPreview";
import { fighterService } from "../services/fightersService";
import { FighterDetail } from "../interfaces/fighter";

type FighterID = string;

export function createFightersSelector() {
  let selectedFighters: [
    FighterDetail | undefined,
    FighterDetail | undefined
  ] = [undefined, undefined];

  return async (event: Event, fighterId: FighterID) => {
    const fighter = (await getFighterInfo(fighterId)) as FighterDetail;
    const [playerOne, playerTwo] = selectedFighters as [
      FighterDetail,
      FighterDetail
    ];
    const firstFighter: FighterDetail = playerOne ?? fighter;
    const secondFighter: FighterDetail = Boolean(playerOne)
      ? playerTwo ?? fighter
      : playerTwo;
    selectedFighters = [firstFighter, secondFighter];

    // check if second fighter chosen
    if (selectedFighters[1]) {
      renderSelectedFighters(
        selectedFighters as [FighterDetail, FighterDetail]
      );
    }
  };
}

const fighterDetailsMap = new Map<FighterID, FighterDetail>();

export async function getFighterInfo(
  fighterId: FighterID
): Promise<FighterDetail | undefined> {
  // get fighter info from fighterDetailsMap or from service and write it to fighterDetailsMap

  if (!fighterDetailsMap.has(fighterId)) {
    const fighter_details: FighterDetail = await fighterService.getFighterDetails(
      fighterId
    );
    fighterDetailsMap.set(fighterId, fighter_details);
  }

  return fighterDetailsMap.get(fighterId);
}

function renderSelectedFighters(
  selectedFighters: [FighterDetail, FighterDetail]
) {
  const fightersPreview = document.querySelector(".preview-container___root");
  const [playerOne, playerTwo] = selectedFighters;
  const firstPreview = createFighterPreview(playerOne, "left");
  const secondPreview = createFighterPreview(playerTwo, "right");
  const versusBlock = createVersusBlock(selectedFighters);

  fightersPreview!.innerHTML = "";
  fightersPreview!.append(firstPreview, versusBlock, secondPreview);
}

function createVersusBlock(selectedFighters: [FighterDetail, FighterDetail]) {
  const canStartFight = selectedFighters.filter(Boolean).length === 2;
  const onClick = () => startFight(selectedFighters);
  const container = createElement({
    tagName: "div",
    className: "preview-container___versus-block",
  });
  const image = createElement({
    tagName: "img",
    className: "preview-container___versus-img",
    attributes: { src: versusImg },
  });
  const disabledBtn = canStartFight ? "" : "disabled";
  const fightBtn = createElement({
    tagName: "button",
    className: `preview-container___fight-btn ${disabledBtn}`,
  });

  fightBtn.addEventListener("click", onClick, false);
  fightBtn.innerText = "Fight";
  container.append(image, fightBtn);

  return container;
}

function startFight(selectedFighters: [FighterDetail, FighterDetail]) {
  renderArena(selectedFighters);
}
