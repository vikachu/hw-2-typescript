import { showModal } from "./modal";
import { createFighterImage } from "../fighterPreview";
import { FighterDetail } from "../../interfaces/fighter";

const restart = () => {
  location.reload();
};

export function showWinnerModal(fighter: FighterDetail) {
  // call showModal function

  const { name: title } = fighter;
  const bodyElement = createFighterImage(fighter);

  showModal({ title, bodyElement, onClose: restart }); // on close - redirect to start;
}
