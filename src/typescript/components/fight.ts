import { controls, criticalHitPauseTime } from "../../constants/controls";
import { FighterDetail } from "../interfaces/fighter";

export async function fight(
  firstFighter: FighterDetail,
  secondFighter: FighterDetail
) {
  return new Promise<FighterDetail>((resolve) => {
    // resolve the promise with the winner when fight is over

    let pressed = new Set<string>();
    let firstFighterCriticalHitLastTime = 0;
    let secondFighterCriticalHitLastTime = 0;

    const leftFighetHealthBar = document.getElementById(
      "left-fighter-indicator"
    );
    const rightFighterHealthBar = document.getElementById(
      "right-fighter-indicator"
    );

    const deleteEventCode = (event: KeyboardEvent) => {
      pressed.delete(event.code);
    };

    const attack = (event: KeyboardEvent) => {
      pressed.add(event.code);
      // Critical Hit
      if (
        checkCriticalHitCombinationPressed(
          controls.PlayerOneCriticalHitCombination,
          pressed
        ) &&
        checkCriticalHitAllowed(firstFighterCriticalHitLastTime)
      ) {
        makeHit(firstFighter, secondFighter, rightFighterHealthBar!, true);
        firstFighterCriticalHitLastTime = Date.now();
      } else if (
        checkCriticalHitCombinationPressed(
          controls.PlayerTwoCriticalHitCombination,
          pressed
        ) &&
        checkCriticalHitAllowed(secondFighterCriticalHitLastTime)
      ) {
        makeHit(secondFighter, firstFighter, leftFighetHealthBar!, true);
        secondFighterCriticalHitLastTime = Date.now();

        // Single Attack
      } else if (
        checkAttackPressed(controls.PlayerOneAttack, pressed) &&
        checkAttackAllowed(
          controls.PlayerOneBlock,
          controls.PlayerTwoBlock,
          pressed
        )
      ) {
        makeHit(firstFighter, secondFighter, rightFighterHealthBar!, false);
      } else if (
        checkAttackPressed(controls.PlayerTwoAttack, pressed) &&
        checkAttackAllowed(
          controls.PlayerTwoBlock,
          controls.PlayerOneBlock,
          pressed
        )
      ) {
        makeHit(secondFighter, firstFighter, leftFighetHealthBar!, false);
      }

      // check defender health
      if (firstFighter.health * secondFighter.health <= 0) {
        const winner = firstFighter.health <= 0 ? secondFighter : firstFighter;
        document.removeEventListener("keydown", attack);
        document.removeEventListener("keydown", deleteEventCode);
        resolve(winner);
      }
    };

    document.addEventListener("keydown", attack);
    document.addEventListener("keyup", deleteEventCode);
  });
}

function checkCriticalHitCombinationPressed(
  combination: Array<string>,
  pressed: Set<string>
) {
  return combination.every((value) => pressed.has(value));
}

function checkCriticalHitAllowed(criticalHitLastTime: number) {
  return Date.now() - criticalHitLastTime > criticalHitPauseTime;
}

function checkAttackPressed(attackerAttackKey: string, pressed: Set<string>) {
  return pressed.has(attackerAttackKey);
}

function checkAttackAllowed(
  attackerBlockKey: string,
  defenderBlockKey: string,
  pressed: Set<string>
) {
  return !(pressed.has(attackerBlockKey) || pressed.has(defenderBlockKey));
}

function makeHit(
  attacker: FighterDetail,
  defender: FighterDetail,
  healthBar: HTMLElement,
  isCriticalHit: Boolean = false
) {
  const damage = isCriticalHit
    ? 2 * attacker.attack
    : getDamage(attacker, defender);
  const newHealthBarWidth = computeHealthBarWidth(
    attacker,
    defender,
    damage,
    healthBar
  );

  healthBar!.style.width = `${newHealthBarWidth}px`;
  defender.health -= damage;
}

export function computeHealthBarWidth(
  attacker: FighterDetail,
  defender: FighterDetail,
  damage: number,
  healthBar: HTMLElement | null
): number {
  const healthBarWidth = healthBar ? healthBar.clientWidth : 0;
  let new_width =
    ((defender.health - damage) * healthBarWidth) / defender.health;
  return new_width > 0 ? new_width : 0;
}

export function getDamage(
  attacker: FighterDetail,
  defender: FighterDetail
): number {
  let damage = getHitPower(attacker) - getBlockPower(defender);
  return damage > 0 ? damage : 0;
}

export function getHitPower(fighter: FighterDetail): number {
  let criticalHitChance = Math.random() + 1;
  let power = fighter.attack * criticalHitChance;
  return power;
}

export function getBlockPower(fighter: FighterDetail): number {
  let dodgeChance = Math.random() + 1;
  let power = fighter.defense * dodgeChance;
  return power;
}
